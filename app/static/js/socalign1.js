function __log(e, data) {
  console.log(e + ' ' + (data || ''));
}

function skipToTrials() {
  $('#instructions').hide();
  $('#testintr').show();
}

$(document).ready(function() {
  var navigator = window.navigator;
    $(':checked').removeAttr('checked');
    $('input[name="browserid"]').val(navigator.userAgent);
    var finished = false;

    var all_trials = $('.testtrial').map(function() {return this.id;}).toArray();

    if (!(Modernizr.canvas && Modernizr.canvastext &&
          Modernizr.getusermedia && Modernizr.audio)) {
      $('#oldBrowserMessage').addClass('overlay').show();
    }

    try {
      navigator.getUserMedia = (
        navigator.getUserMedia ||
          navigator.webkitGetUserMedia ||
          navigator.mozGetUserMedia ||
          navigator.msGetUserMedia);
      audio_context = new AudioContext();
      console.log('Audio context set up.');

      navigator.getUserMedia({audio: true},
          function(stream) {
            //we have a mice, set up the recorder and start the hit.
              startRecorder(stream);
            },
          function(e) {
            //maybe also issue alert that this hit should not be accepted as there was an issue getting the audio stream setup?
            //For now if the audio stream doesn't work than the hit doesn't advance but with no error message
              __log('No live audio input: ' + e);
            }
      );

      } catch (e) {
          alert('There is no audio recording support in this browser, do not accept this HIT!');
      }

    if (Modernizr.audio) {
        $('#instructions').show();

        $('#voladjust').on('play', function() {this.volume = 1;});
        $('#voladjust').on('volumechange', function() {this.volume = 1;});
        $('#voladjust').on('ended', function() {this.currentTime = 0; this.pause();});

        if (debugmode) {
            $('#exposure audio').attr('controls', true);
        } else {
            $('#exposure audio').attr('controls', false);
        }

        $('#exposure audio').on('play', function() {
            if (typeof(console) !== undefined) {console.log('Audio playing');}
        });

        $('#exposure audio').on('ended', function() {
            if (typeof(console) !== undefined) {console.log('Audio ended');}
            //$(':input[name="endaudio"]').val(new Date().toISOString());
            $('#headphones').hide();
            $('#exposure').hide();
            $('#testintr').show();
        });
    }

    $('button#reset').on('click', function() {
        $.ajax({
            type: 'POST',
            url: '/',
            data: {'ItemNumber': 0, 'Abandoned': false, 'WorkerId': workerId},
            datatype: 'json'
        }).done(function() {
            alert('Reset to zero. Reload the page to start from scratch.');
        }).fail(function() {
            alert('Failed to reset to zero.');
        });
    });

    $('button#startrecordtest').on('click', function() {
      $(this).attr('disabled', 'disabled');
      $('button#endrecordtest').removeAttr('disabled');
      $('#micwarning').show();
      displayAudioMeter();
    });

    $('button#endrecordtest').on('click', function() {
        $(this).attr('disabled', 'disabled');
        $('button#startrecordtest').removeAttr('disabled');
        $('button#endsetup').removeAttr('disabled');
        disconnectAudioMeter();
    });

    $('button#endsetup').on('click', function() {
        $('#audiosetup').hide();
        $('#realinstructions').show();
    });

    $('button#endinstr').on('click', function() {
      //$(':input[name="starttime"]').val(new Date().toISOString());
      $('#instructions').hide();
      $('#exposure').show(function() {
          $('#exposure audio')[0].volume = 1;
          $('#exposure audio')[0].play();
          $('#headphones').show();
      });
    });

    $('button#starttest').on('click', function() {
        $('#testintr').hide();
        $('.testtrial').first().show(function() {
            recorder.record();
            $('#microphone').show();
        });
    });

    $('button#resume').on('click', function() {
      $('#reloadResume').hide();
      if (itemno == $('.testtrial').length) {
          $('#page1').show();
      } else {
          $($('.testtrial')[itemno]).show(function() {
            recorder.start();
          });
      }
    });

    $('.stoprecord').on('click', function() {
      recorder.stop();
      $('#microphone').hide();
      $('#cloud').show();
      console.log($('.testtrial:visible').attr('id'));
      recorder.exportWAV(function(blob) {
        // http://jsfiddle.net/GvdSy/
        $.ajax({
          xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener('progress', function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                console.log(percentComplete);
                $('.progress').css({
                  width: percentComplete * 100 + '%'
                });
                if (percentComplete === 1) {
                  $('.progress').addClass('hide');
                }
              }
            }, false);
            xhr.addEventListener('progress', function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                console.log(percentComplete);
                $('.progress').css({
                  width: percentComplete * 100 + '%'
                });
              }
            }, false);
            return xhr;
          },
          type: 'PUT',
          url: $('.testtrial:visible a').attr('href'),
          data: blob,
          processData: false,
          contentType: 'audio/x-wav',
          success: function(data, textStatus, jqXHR) {
            recorder.clear();
            $('#cloud').hide();
            $('.progress').css({
              width: 0
            }).removeClass('hide');
            console.log(textStatus, data);
            nextTrial();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });
      });
    });

    var nextTrial = function() {
      var current = '#' + all_trials.shift();
      $(current).hide('slide');
      if (all_trials.length === 0) {
        console.log('Last trial ' + current + ' -> survey');
        $('#page1').show();
      } else {
        var next = '#' + all_trials[0];
        console.log('Showing next trial ' + current + ' -> ' + next);
        $(next).show('slide');
        recorder.record();
        $('#microphone').show();
      }
    };

    // FIXME: this seems stranded
    // if (itemno === 0) { // this should happen only if starting from the beginning
    //     $('#instructions').show();
    //     } else { // and this should only happen if we're starting from after the 1st item
    //         $('#instructions').hide();
    //         $('#reloadResume').show();
    // }

});


// TODO: Figure out if this is still useful, even partially and integrate it back in
// var onRecordFinishUpdate = function() {
//     clearInterval(recordInterval);
//     $('button.stoprecord:visible').siblings('.hiddennext').click();
//     $.ajax({
//             type: 'POST',
//             url: '/mturk/experiments/socalign1',
//             data: {'ItemNumber': ++itemno, 'WorkerId': workerId},
//             datatype: 'json'
//         }).done(function(msg) {
//             if (debugmode) {
//                 console.log('Updated to ' + JSON.stringify(msg));
//             }
//     });
// };
