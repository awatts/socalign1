#!/usr/bin/env python

# Author: Andrew Watts
#
#    Copyright 2009-2013 Andrew Watts and
#        the University of Rochester BCS Department
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License version 2.1 as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.
#    If not, see <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>.
#

from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

from random import shuffle
import json
import yaml
import os
from datetime import datetime
import time
from functools import partial
from six.moves import configparser
from six.moves.configparser import NoSectionError, NoOptionError
from werkzeug.exceptions import MethodNotAllowed, BadRequest, abort
from flask import render_template, request, url_for, g
from flask import jsonify
from flask.ext.assets import Environment
from jinja2.exceptions import TemplateNotFound
from sqlalchemy.orm.exc import NoResultFound
from itsdangerous import URLSafeSerializer, BadSignature
from .models import Worker
from .utils import get_or_create_worker_assignment, random_list, static_list
from app import app
from app import db

assets = Environment(app)

basepath = os.path.dirname(__file__)


@app.template_filter('shuffle')
def shuffle_filter(value):
    shuffle(value)
    return value


# @app.errorhandler(400)
# def bad_request(error):
#     return render_template('400.html', error=error), 400
#
#
# @app.errorhandler(404)
# def page_not_found(e):
#     return render_template('404.html'), 404
#
#
# @app.errorhandler(405)
# def method_not_allowed(e):
#     """405 Method Not Allowed"""
#     return render_template('405.html'), 405


@app.before_request
def before_request():
    """
    http://stackoverflow.com/q/12273889/3846301
    """
    g.start = time.time()


@app.after_request
def after_request(response):
    """
    Based on http://stackoverflow.com/a/15204387/3846301
    """
    diff = int((time.time() - g.start) * 1000)  # to get a time in ms

    if response.response and response.content_type.startswith("text/html") and response.status_code == 200:
        response.headers.add('X-Flask-Response-Time', '{}ms'.format(str(diff)))

    return response


@app.context_processor
def utility_processor():
    return dict(get_upload_link=get_upload_link)


# Some parts based on http://flask.pocoo.org/snippets/50/
def get_serializer(secret_key=None):
    if secret_key is None:
        secret_key = app.secret_key
    return URLSafeSerializer(secret_key)


def get_upload_link(experiment, workerid, assignmentid, trial):
    s = get_serializer()
    payload = s.dumps((experiment, workerid, assignmentid, trial))
    return url_for('wav_uploader', payload=payload, _external=True)


@app.route('/wav_uploader/<payload>', methods=['PUT', ])
def wav_uploader(payload):
    """
    Handle WAV uploads
    """
    # TODO: should only allow XHR
    # print(request.headers)
    s = get_serializer()
    try:
        experiment, workerid, assignmentid, trial = s.loads(payload)
        print('{},{},{}'.format(workerid, assignmentid, trial))
    except BadSignature:
        print('bad signature')
        raise BadRequest('Invalid file signature')

    if request.content_type != 'audio/x-wav':
        print('need wavs')
        raise BadRequest('Only WAV files can be uploaded')

    savepath = os.path.join(basepath, 'uploads', experiment, workerid, assignmentid)
    savefile = trial + ".wav"

    if not os.path.exists(savepath):
        os.makedirs(savepath)

    with open(os.path.join(savepath, savefile), 'wb') as wavfile:
        wavfile.write(request.data)

    return 'Uploaded "{}"'.format(savefile)


@app.route('/update_position')
def update_position():
    if request.is_xhr:
        if not request.method == 'POST':
            raise MethodNotAllowed(valid_methods=('POST',))

        if 'WorkerId' not in request.args:
            raise BadRequest('Missing key: WorkerId')

        worker = Worker.get_object_or_404(workerid=request.args['WorkerId'])

        if 'ItemNumber' in request.args:
            worker.lastseen = datetime.now()
            worker.lastitem = request.args['ItemNumber']
            db.session.add(worker)
            # print("Setting {} to {} at {}".format(worker.workerid, worker.lastitem, worker.lastseen))

        if 'Abandoned' in request.args:
            worker.abandoned = request.args['Abandoned'] == "true"
            db.session.add(worker)

        db.session.commit()

        return jsonify(timestamp=worker.lastseen.isoformat(), item=worker.lastitem)


#  http://stackoverflow.com/a/13318415/3846301
def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@app.route("/site-map")
def site_map():
    links = []
    for rule in app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods and has_no_empty_params(rule):
            url = url_for(rule.endpoint)
            links.append((url, rule.endpoint))
            # links is now a list of url, endpoint tuples
    return render_template("all_links.jinja2", links=links)


@app.route('/socalign1')
def soc_align1_server():
    """Dispatch pages for SocAlign1 experiment"""

    cfg = configparser.ConfigParser()
    cfg['form'] = {'type': 'testing'}
    cfg.read(os.path.join(basepath, 'expt.cfg'))

    formtype = cfg.get('form', 'type')

    # read in the stimuli via JSON
    # FIXME: find a less stupid way to store and load stimuli
    stims = []
    with open(os.path.join(basepath, 'stims.json'), 'r', encoding='utf-8') as p:
        stims = json.load(p)

    amz_dict = {'workerId': '', 'assignmentId': '', 'hitId': ''}
    listid, condition = None, None
    required_keys = {'assignmentId', 'hitId'}
    key_error_msg = 'Missing parameter: {0}. Required keys: {1}'

    debug = False
    if 'debug' in request.args:
        debug = True if request.args['debug'] == '1' else False

    forcelist = None
    if 'list' in request.args:
        forcelist = int(request.args['list'])

    try:
        amz_dict['assignmentId'] = request.args['assignmentId']
        amz_dict['hitId'] = request.args['hitId']
    except KeyError as e:
        raise BadRequest(key_error_msg.format(e, required_keys))

    in_preview = True if amz_dict['assignmentId'] == 'ASSIGNMENT_ID_NOT_AVAILABLE' else False

    worker = None
    if not in_preview:
        try:
            amz_dict['workerId'] = request.args['workerId']
        except KeyError as e:
            required_keys.add('workerId')
            raise BadRequest(key_error_msg.format(e, required_keys))
        worker, existing_worker = Worker.get_one_or_create(workerid=amz_dict['workerId'])
        if not existing_worker:
            worker.triallist = random_lowest_list()
            db.session.add(worker)
            db.session.commit()

    currlist, soundtrials, pictrials = [[] for x in range(3)]
    condition, survey = None, None
    if worker:
        if debug and forcelist:
            listid = forcelist
        else:
            listid = worker.triallist.number
        currlist = [x for x in stims if int(x['List']) == listid]
        soundtrials = [y for y in currlist if y['TrialType'] == 'EXPOSURE']
        pictrials = [z for z in currlist if z['TrialType'] == 'TEST']
        # cond is same for all pictrials in a list; grab from 1st
        condition = pictrials[0]['ExposureCondition']
        survey = pictrials[0]['SurveyList']
    else:
        soundtrials.append(None)

    template_file = 'socalign1.html'
    startitem = 0
    if worker is not None:
        startitem = worker.lastitem

    return render_template(template_file,
                           soundfile=soundtrials[0],  # only one sound file
                           pictrials=pictrials,
                           amz=amz_dict,
                           listid=listid,
                           survey=survey,
                           condition=condition,
                           formtype=formtype,
                           debugmode=1 if debug else 0,
                           startitem=startitem,
                           # on preview, don't bother loading heavy flash assets
                           preview=in_preview)


@app.route('/')
@app.route('/socalign2pilot')
def soc_align2_pilot():
    """Dispatch pages for SocAlign2 pilot experiment"""

    condition = None
    required_keys = {'assignmentId', 'hitId', 'experiment'}
    key_error_msg = 'Missing parameter: {0}. Required keys: {1}'

    if 'debug' in request.args:
        app.debug = request.args['debug'] == '1'

    missing_keys = set()
    for k in required_keys:
        if k not in request.args:
            missing_keys.add(k)
    if missing_keys:
        abort(400, {'message': key_error_msg.format(missing_keys, required_keys)})

    cfg = configparser.SafeConfigParser()
    config_expt = request.args['experiment']
    cfg_file = cfg.read(os.path.join(basepath, 'experiment_config', '{}.cfg'.format(config_expt)))
    if not cfg_file:
        abort(400, dict(message='{}.cfg does not exist'.format(config_expt)))
    try:
        listtype = cfg.get('lists', 'type')
    except (NoSectionError, NoOptionError) as e:
        abort(400, {'message': 'In {}: {}'.format(cfg_file, e.message)})

    if listtype == 'filtered':
        lsubset = cfg.get('lists', 'lists').strip().split(',')
    elif listtype == 'static':
        lname = cfg.get('lists', 'list')

    formtype = None
    if 'dest' in request.args:
        if request.args['dest'] in ('mturk', 'sandbox'):
            formtype = request.args['dest']
        else:
            abort(400, {'message': "{} is not a valid form destination".format(request.args['dest'])})

    in_preview = True if request.args['assignmentId'] == 'ASSIGNMENT_ID_NOT_AVAILABLE' else False

    assignment = None
    if not in_preview:
        if 'workerId' not in request.args:
            missing_keys.add('workerId')
            required_keys.add('workerId')
            abort(400, {'message': key_error_msg.format(missing_keys, required_keys)})
        get_assignment = partial(get_or_create_worker_assignment,
                                 request.args['experiment'],
                                 request.args['workerId'],
                                 request.args['assignmentId'])
        if listtype == 'random':
            assignment = get_assignment('random')
        elif listtype == 'filtered':
            assignment = get_assignment('filtered', lsubset=lsubset)
        elif listtype == 'static':
            assignment = get_assignment('static', listname=lname)

    stimuli = []
    if assignment:
        condition = "{}/{}".format(request.args['experiment'], assignment.triallist.name)
        with open(os.path.join(basepath, 'stimuli',
                               request.args['experiment'],
                               '{}.yml'.format(assignment.triallist.name)), 'r') as stimfile:
            stimuli = yaml.load(stimfile)
    else:
        stimuli = [{'stimulus': ''}]

    template_file = '{}.html'.format(request.args['experiment'])
    try:
        return render_template(template_file,
                               amz=request.args,
                               experiment=request.args['experiment'],
                               condition=condition,
                               # survey=survey,  # FIXME: set this somehow
                               formtype=formtype,
                               trials=stimuli,
                               debug=1 if app.debug else 0,
                               preview=in_preview)
    except TemplateNotFound as e:
        abort(400, {'message': '{}'.format(e.message)})


@app.route('/demo/<experiment>')
@app.route('/demo/<experiment>/<listname>')
def socalign_demo(experiment, listname=None):
    """
    Demo version of SocAlign experiments. Doesn't hit db.
    """

    if listname:
        triallist = static_list(experiment, listname)
    else:
        triallist = random_list(experiment)

    amz = {
        'experiment': experiment,
        'assignmentId': 'NA',
        'workerId': '{}_{}_demo'.format(experiment, triallist.name)
    }

    stimuli = []
    condition = "{}/{}".format(experiment, triallist.name)
    with open(os.path.join(basepath, 'stimuli', experiment,
                           '{}.yml'.format(triallist.name)), 'r') as stimfile:
        stimuli = yaml.load(stimfile)

    template_file = '{}.html'.format(experiment)
    try:
        return render_template(template_file,
                               amz=amz,
                               experiment=experiment,
                               condition=condition,
                               formtype='local',
                               trials=stimuli,
                               debug=0,
                               preview=False)
    except TemplateNotFound as e:
        abort(400, {'message': '{}'.format(e.message)})


@app.route('/echo', methods=['POST'])
def response_repeater():
    """
    Take all the keys and values in a request object and print them in a table
    """
    return render_template('echo.jinja2', formdata=request.form)
