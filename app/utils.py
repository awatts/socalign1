#
# Author: Andrew Watts <awatts@bcs.rochester.edu>
#
# Copyright (c) 2014, Andrew Watts and
#        the University of Rochester BCS Department
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import absolute_import

from random import choice
from collections import namedtuple, Counter
from .models import (
    Worker,
    Assignment,
    TrialList,
    Experiment
)
from app import db
from .notifications import process_notifications

ListCount = namedtuple('ListCount', ['ListId', 'Count'])


def get_or_create_worker_assignment(experiment, workerid, assignmentid, chooselist='random', **kwargs):
    """
    Looks for a Worker with a given MTurk workerid in the database, creating one
    if necessary. Then looks for an Assigment with a given assignmentid in the
    database, creating one if necessary, and adding the worker and a list by the
    selected method.
    :param experiment: Name of the experiment
    :param workerid: WorkerId to find or create
    :param assignmentid: AssignmentId to find or create
    :param chooselist:  How to choose a list for the worker: 'random' (default), 'filtered', or 'static'
    :param kwargs: Keyword arguments to pass on to list choosing functions
    """
    worker, w_existed = Worker.get_one_or_create(workerid=workerid)
    expt = Experiment.query.filter_by(name=experiment).first_or_404()

    # This seems like the best point to check the notification queue and
    # make sure assignment statuses are up to date
    # TODO: instead, set up a timer to check notifications every 30 seconds until a HITReviewable
    # event with the right HITTypeId comes up. Maybe add a refcounter so if multiple experiments
    # are running, we don't cancel while others need to be pumping the queue. Alternately: Add a
    # property to HITTypeIds for whether they are active, set to true on creation and on a HITExtended
    # event and false on a HITReviewable or HITExpired event
    process_notifications()

    # If a a notification event created an assignment, it might not have worker and experiment set
    # so just search / create on assignmentid
    assignment, a_existed = Assignment.get_one_or_create(assignmentid=assignmentid)

    if not assignment.worker:
        assignment.worker = worker
    if not assignment.experiment:
        assignment.experiment = expt
    db.session.add(assignment)
    db.session.commit()
    if not assignment.triallist:
        if chooselist == 'random':
            assignment.triallist = random_lowest_list(experiment)
        elif chooselist == 'filtered':
            assignment.triallist = random_lowest_list(experiment, kwargs['lsubset'])
        elif chooselist == 'static':
            assignment.triallist = static_list(experiment, kwargs['listname'])
    db.session.add(assignment)
    db.session.commit()

    return assignment


# def random_lowest_list(experiment, listsubset=None):
#     """
#     Randomly selects a trial list from all available lists. If listsubset is
#     a sequence of list names, restrict to those lists
#     """
#     expt = Experiment.query.filter_by(name=experiment).one()
#
#     # We don't want to count abandoned or rejected HIT assignments in the balance count
#     activeassn = Assignment.query.filter(
#         Assignment.experiment == expt,
#         Assignment.state.notin_(['Abandoned', 'Returned', 'Rejected']),
#         Assignment.list_id != None)
#
#     # This gets rid of all the list_id = Nones, otherwise listlens assignment breaks on PostgreSQL
#     # n.b. seems to break with PostgreSQL no matter what
#     activeassn = Assignment.query.filter(Assignment.assignmentid.in_([a.assignmentid for a in activeassn.all()]))
#
#     # restrict to assignments with lists in the specified subset if given
#     expt_lists = TrialList.query.filter(TrialList.experiment == expt)
#     if listsubset:
#         lsubset = [x.id for x in expt_lists.filter(
#             TrialList.name.in_(listsubset)).all()]
#     else:
#         lsubset = [x.id for x in expt_lists.all()]
#
#     # make a list of (list, count) tuples for selected lists, sorted by count
#     # n.b. MySQL/MariaDB breaks here. Why did I use is_() instead of == ?
#     #listlens = sorted([(y, activeassn.filter(Assignment.list_id.is_(y)).count()) for y in lsubset], key=lambda x: x[1])
#     listlens = sorted([ListCount(y, activeassn.filter(Assignment.list_id == y).count()) for y in lsubset], key=lambda x: x.Count)
#
#     # discard any lists with a count greater than the list with the lowest count
#     lowcount = listlens[0].Count
#     for i, x in enumerate(listlens):
#         if x.Count > lowcount:
#             listlens = listlens[:i]
#             break
#
#     # If there's only one left, use that list id, else random choose one
#     if len(listlens) == 1:
#         lowlist = listlens[0].ListId
#     else:
#         lowlist = choice([x.ListId for x in listlens])
#
#     return TrialList.query.get(lowlist)

def random_lowest_list(experiment, listsubset=None):
    """
    Randomly select a trial list from all available lists.

    If listsubset is a sequence of list names, restrict to those lists

    :param experiment: Name of the experiment
    :param listsubset: Sequence of list names
    :return a TrialList
    """
    expt = Experiment.query.filter_by(name=experiment).one()

    # restrict to assignments with lists in the specified subset if given
    expt_lists = TrialList.query.filter(TrialList.experiment == expt)

    if listsubset:
        expt_lists = expt_lists.filter(TrialList.name.in_(listsubset))

    list_counts = Counter(
        {x.id: len([y for y in x.assignments if y.state in {'InProcess', 'Submitted', 'Approved'}])
            for x in expt_lists.all()})

    if list_counts.most_common():
        # count element of last list element
        lowcount = list_counts.most_common(len(list_counts))[-1][1]
        lowlist = choice([k for k, v in list_counts.items() if v <= lowcount])
        return TrialList.query.get(lowlist)
    else:  # on the first assignment for an experiment pick any list
        return random_list(experiment)


def random_list(experiment):
    """
    :param experiment:
    :return: Any TrialList from experiment

    Randomly selects any TrialList from a given Experiment
    """
    expt = Experiment.query.filter_by(name=experiment).one()
    all_lists = TrialList.query.filter_by(experiment=expt).all()
    return choice(all_lists)


def static_list(experiment, listnameid):
    expt = Experiment.query.filter_by(name=experiment).one()
    filtered_list = TrialList.query.filter(TrialList.experiment == expt)
    if isinstance(listnameid, int):
        return filtered_list.filter(TrialList.id == listnameid).one()
    else:
        return filtered_list.filter(TrialList.name == listnameid).one()
