#
# Author: Andrew Watts <awatts@bcs.rochester.edu>
#
# Copyright (c) 2014, Andrew Watts and
#        the University of Rochester BCS Department
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import absolute_import, division

from math import ceil
from boto import sqs
import json
from dateutil.parser import parse
from config import SQS_REGION, SQS_QUEUE, AWS_ACCESS_KEY, AWS_SECRET_KEY
from app import db
from .models import NotificationEvent, HITId, HITTypeId, Assignment

assignment_map = {
    'AssignmentAccepted': 'InProcess',
    'AssignmentAbandoned': 'Abandoned',
    'AssignmentReturned': 'Returned',
    'AssignmentSubmitted': 'Submitted'
}


def process_notifications():
    """
    Fetch all notification from the SQS queue, create NotificationEvents for each
    and update the state of any affected Assignments
    """
    conn = sqs.connect_to_region(SQS_REGION, aws_access_key_id=AWS_ACCESS_KEY, aws_secret_access_key=AWS_SECRET_KEY)
    # FIXME: Should do something more helpful than silently fail and return
    if conn is None:
        return
    my_queue = conn.get_queue(SQS_QUEUE)
    if my_queue is None:
        return

    qcount = my_queue.count()
    if qcount > 0:
        #  MaxNumberOfMessages must be between 1 and 10, so loop if > 10
        messages = []
        for _ in range(int(ceil(qcount / 10))):
            messages.extend(my_queue.get_messages(10))

        # FIXME: some exception handling should happen in here
        for msg in messages:
            mdict = json.loads(msg.get_body())
            docid = mdict['EventDocId']
            for e in mdict['Events']:
                hittypeid, _ = HITTypeId.get_one_or_create(hittypeid=e['HITTypeId'])
                hitid, _ = HITId.get_one_or_create(hitid=e['HITId'], hittypeid=hittypeid)
                note_event, _ = NotificationEvent.get_one_or_create(event_type=e['EventType'],
                                                                    eventdocid=docid,
                                                                    hittypeid=hittypeid,
                                                                    hitid=hitid,
                                                                    event_time = parse(e['EventTimestamp'])
                                                                    )
                assignment = None
                if e['EventType'] in ('AssignmentAccepted', 'AssignmentAbandoned',
                                      'AssignmentReturned', 'AssignmentSubmitted'):
                    assignment, _ = Assignment.get_one_or_create(assignmentid=e['AssignmentId'])
                    if assignment.state != assignment_map[e['EventType']]:
                        assignment.state = assignment_map[e['EventType']]
                        db.session.add(assignment)
                        db.session.commit()
                # Some event types are HIT based, not assignment based
                # Only add the assignment to the NotificationEvent is new
                # and has no assignment connected to it
                if assignment and note_event.assignment is None:
                    note_event.assignment = assignment
                    db.session.add(note_event)
                    db.session.commit()
        my_queue.delete_message_batch(messages)

# example SQS notification with event
# {u'CustomerId': u'A29979FPDJ98R8',
#  u'EventDocId': u'2d63dbfd28a75a32325390ed2acd193dbe7cdfcd',
#  u'EventDocVersion': u'2006-05-05',
#  u'Events': [{u'AssignmentId': u'1234567890123456789012345678901234567890',
#               u'EventTimestamp': u'2014-11-19T18:58:16Z',
#               u'EventType': u'AssignmentAbandoned',
#               u'HITId': u'12345678901234567890',
#               u'HITTypeId': u'09876543210987654321'},
#              {u'AssignmentId': u'1234567890123456789012345678900987654321',
#               u'EventTimestamp': u'2014-11-19T18:58:16Z',
#               u'EventType': u'AssignmentAbandoned',
#               u'HITId': u'12345678901234567890',
#               u'HITTypeId': u'09876543210987654321'}]}
