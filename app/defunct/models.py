#!/usr/bin/env python

# Author: Andrew Watts
#
#    Copyright 2009-2016 Andrew Watts and the University of Rochester
#    Brain and Cognitive Sciences Department
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License version 2.1 as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.
#    If not, see <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>.

"""
Database models for Kathryn Campbell-Kibler, Kodi Weatherholtz, and T. Florian
Jaeger's experiment. Based onCamber Hansen-Karr's EmoPrm1 experiment
"""
import datetime

from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from werkzeug.exceptions import abort
from app import db


class MyMixin(object):

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def get_one_or_create(cls, create_method='',
                          create_method_kwargs=None,
                          **kwargs):
        """
        Get the db instance of the kwarg specified object or create
        if necessary. Returns a tuple of the object and whether it
        already existed (True) or was created (False)
        Modified from:
        http://skien.cc/blog/2014/01/15/sqlalchemy-and-race-conditions-implementing/
        """
        try:
            return cls.query.filter_by(**kwargs).one(), True
        except NoResultFound:
            kwargs.update(create_method_kwargs or {})
            created = getattr(cls, create_method, cls)(**kwargs)
            try:
                db.session.add(created)
                db.session.commit()
                return created, False
            except IntegrityError:
                db.session.rollback()
                return cls.query.filter_by(**kwargs).one(), True
        except MultipleResultsFound:
            # FIXME: Multiples should never happen
            # In the case they do it should somehow warn so it can get fixed
            # short term solution is to just always return the first one so
            # everything gets assigned that one and it's easier to delete dups
            return cls.query.filter_by(**kwargs).first(), True

    @classmethod
    def get_object_or_404(cls, **kwargs):
        try:
            return cls.query.filter_by(**kwargs).one()
        except (NoResultFound, MultipleResultsFound) as e:
            abort(404, {'message': e})


class Worker(db.Model, MyMixin):
    """
    Representation of an mturk worker and what list they were assigned to
    """

    __tablename__ = 'worker'

    workerid = db.Column(db.String(32), unique=True)
    abandoned = db.Column(db.Boolean, default=False)
    lastitem = db.Column(db.Integer, default=0)
    firstseen = db.Column(db.DateTime, default=datetime.datetime.now, nullable=False)
    lastseen = db.Column(db.DateTime, default=datetime.datetime.now, nullable=False)
    list_id = db.Column(db.Integer, db.ForeignKey('triallist.id'))
    triallist = db.relationship('TrialList', backref='workers')

    def __repr__(self):
        return '<Worker: "%s">' % (self.workerid)


class TrialList(db.Model, MyMixin):
    """
    Which list workers are on
    """

    __tablename__ = 'triallist'

    number = db.Column(db.Integer)

    def __repr__(self):
        return '<TrialGroup: "%d">' % (self.number)

    @property
    def active_workers(self):
        return [x for x in self.workers if not x.abandoned]
