#!/usr/bin/env python

# Author: Andrew Watts <awatts2@ur.rochester.edu>
#
#    Copyright 2016 Andrew Watts and
#        the University of Rochester BCS Department
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License version 2.1 as
#    published by the Free Software Foundation.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.
#    If not, see <http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html>.

"""
Read in a Mechanical Turk tab delimited file from Kodi Weatherholtz's SoclAlign2
pilot experiment and generate a one trial per line csv file
"""

import json
from dateutil.parser import parse
import pandas as pd


def millis_interval(start, end):
    """start and end are datetime instances"""
    diff = end - start
    millis = diff.days * 24 * 60 * 60 * 1000
    millis += diff.seconds * 1000
    millis += diff.microseconds / 1000
    return millis

hlpfields = ['experiment', 'list', 'comment', 'browserid']

questionfields = ['q.speaker.conservative',
                  'q.speaker.liberal',
                  'q.speaker.articulate',
                  'q.speaker.accented',
                  'q.speaker.intelligent',
                  'q.speaker.educated',
                  'q.speaker.self-centered',
                  'q.speaker.generous',
                  'q.speaker.weak_arguments',
                  'q.speaker.shy',
                  'q.speaker.enthusiastic',
                  'q.speaker.easy_to_understand',
                  'q.participant.speaker_speaks_like_me',
                  'q.participant.speaker_is_similar_to_me',
                  'q.participant.speaker_would_understand_me',
                  'q.participant.agree_with_speaker',
                  'q.participant.want_speaker_as_friend',
                  'q.participant.education',
                  'q.ideology.conservative',
                  'q.ideology.liberal',
                  'q.ideology.republicans',
                  'q.ideology.democrats',
                  'q.ideology.enjoy_accents',
                  'q.ideology.proper_english',
                  'q.ideology.official_language',
                  'q.ideology.importance_of_speaking_well',
                  'q.ideology.accent_and_self-presentation',
                  'q.ideology.accent_predicts_intelligence',
                  'q.conflict.dominate.my_way_best',
                  'q.conflict.avoid.ignore',
                  'q.conflict.integrate.meet_halfway',
                  'q.conflict.dominate.insist_my_position_be_accepted',
                  'q.conflict.avoid.pretend_nothing_happend',
                  'q.conflict.avoid.pretend_no_conflict',
                  'q.conflict.integrate.middle_course',
                  'q.conflict.dominate.dominate_until_other_understands',
                  'q.conflict.integrate.give_and_take']

df = pd.read_csv('mturk/socalign2pilot.results.combined.csv', sep='\t')

df['Answer.comment'].fillna('', inplace=True)

trial_rows = []

for index, row in df.iterrows():
    responses = json.loads(row['Answer.trials'])
    for k in responses.keys():
        trial = {}
        trial['trialname'] = k
        trial['response'] = responses[k]['choice']
        trial['trialtime'] = millis_interval(parse(responses[k]['startTime']), parse(responses[k]['stopTime']))
        trial['assignmentid'] = row['assignmentid']
        trial['workerid'] = row['workerid']
        trial['q.participant.gender'] = row['Answer.rsrb.sex']
        trial['q.participant.age'] = row['Answer.rsrb.age']
        print(trial['workerid'])
        for h in hlpfields:
            trial[h] = row['Answer.{}'.format(h)]
        for q in questionfields:
            trial[q] = row['Answer.{}'.format(q)]
        trial_keys = [key for key in df.keys() if key.startswith('Answer.{}.'.format(k))]
        for t in trial_keys:
            _, trial_name, trial_var = t.split('.')
            trial[trial_var] = row[t]
        trial_rows.append(trial)

converted = pd.DataFrame(trial_rows)
converted.to_csv('socalign2pilot.converted.csv', index=False)
