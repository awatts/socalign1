import os
basedir = os.path.abspath(os.path.dirname(__file__))

ASSETS_DEBUG = False  # set to True to output individual asset files instead of merging

SECRET_KEY = "CHANGE THIS TO SOMETHING RANDOM"  # Flask suggests contents of os.urandom(24)

MAX_CONTENT_LENGTH = 16 * 1024 * 1024  # 16MB max file upload size

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'socalign1.sqlite')
SQLALCHEMY_TRACK_MODIFICATIONS = False

UGLIFYJS_EXTRA_ARGS = 'screw-ie8'
